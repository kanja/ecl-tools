#!/usr/bin/env/python

from setuptools import setup

setup(
    name = 'ecl_tools',
    version = '0.4.1',
    url = 'http://elmcitylabs.com',
    license = 'BSD',
    description = 'Some useful utilities for Python',
    author = 'Dan Loewenherz',
    author_email = 'dan@elmcitylabs.com',
    packages=['ecl_tools'],
    install_requires=['redis==2.4.11'],
)

